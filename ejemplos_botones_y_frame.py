import tkinter as tk

root = tk.Tk()
root.title("Ejemplo de Pack")

# Creamos un Frame
frame = tk.Frame(root)
frame.pack()

# Creamos varios botones y los empaquetamos dentro del Frame
boton1 = tk.Button(frame, text="Botón 1")
boton1.pack(side="left")

boton2 = tk.Button(frame, text="Botón 2")
boton2.pack(side="left")

boton3 = tk.Button(frame, text="Botón 3")
boton3.pack(side="left")

root.mainloop()
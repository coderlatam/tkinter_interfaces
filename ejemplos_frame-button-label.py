import tkinter as tk

# Función de ejemplo para un botón dentro del frame
def mostrar_mensaje():
    label.config(text="¡Hola desde el Frame!")

# Crear la ventana principal
root = tk.Tk()
root.title("Ejemplo de Frame")

# Crear un frame
frame = tk.Frame(root, padx=20, pady=20)  # Puedes ajustar el padding según tus necesidades
frame.pack()

# Agregar un botón dentro del frame
button = tk.Button(frame, text="Mostrar Mensaje", command=mostrar_mensaje)
button.pack()

# Agregar una etiqueta dentro del frame
label = tk.Label(frame, text="Este es un Frame en Tkinter")
label.pack()

# Ejecutar el bucle principal
root.mainloop()

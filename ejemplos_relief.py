import tkinter as tk

root = tk.Tk()
root.title("Ejemplo de Relief")

frame1 = tk.Frame(root, width=100, height=100, relief="raised", borderwidth=2)
frame1.pack(padx=10, pady=10)

frame2 = tk.Frame(root, width=100, height=100, relief="sunken", borderwidth=2)
frame2.pack(padx=10, pady=10)

frame3 = tk.Frame(root, width=100, height=100, relief="groove", borderwidth=2)
frame3.pack(padx=10, pady=10)

frame4 = tk.Frame(root, width=100, height=100, relief="ridge", borderwidth=2)
frame4.pack(padx=10, pady=10)

frame5 = tk.Frame(root, width=100, height=100, relief="flat", borderwidth=2)
frame5.pack(padx=10, pady=10)

root.mainloop()
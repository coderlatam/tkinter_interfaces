'''
sudo apt-get install python3-tk

python --version
pip -V
pip install tk

'''


import tkinter as tk
'''
Defino la raiz
'''
raiz = tk.Tk()

'''
    A mi ventana le agrego un titulo de prueba.

'''
raiz.title("Mi primera ventana")
'''
    Para impedir que se permita redimencionar la ventana usamos resizable(width,height)
    Se puede usar (0,0) || (True,False) || (False,False)
'''
#raiz.resizable(0,0)
'''
    Cambiar el icono de nuestra aplicacion por que quisieramos, debe ser .ico
'''
#icono = tk.PhotoImage(file="gugler.png")
#raiz.iconphoto(False, icono)

#raiz.iconbitmap('users_icon.ico')
'''
    Cambiar el tamaño de la ventana
'''
#raiz.geometry("650x380")
'''
    Cambiar el color  de fondo de mi interfaz
'''
raiz.config(bg="blue")

'''
    Declarar o definir un frame y empaquetarlo con la raiz
'''
miFrame = tk.Frame()
# miFrame.pack(side="left", anchor="n")
miFrame.pack(fill="y", expand=True)
'''
    Configuramos color de fondo y tamaño
'''
miFrame.config(bg="red")
miFrame.config(width="650", height="350")

'''
    relief para diferenciar bordes de frames, botones,etc.

'''
miFrame.config(bd=35)
miFrame.config(relief="groove")
'''
    podemos diferenciar el cursor de la raiz y el/los frame
'''
miFrame.config(cursor="hand2")
'''
    mainloop es para que la ventana se quede abierta
    todas las instrucciones deben ir por encima de mainloop
'''
raiz.mainloop()
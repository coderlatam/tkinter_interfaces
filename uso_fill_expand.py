import tkinter as tk

root = tk.Tk()
root.title("Ejemplo de Pack")

# Creamos dos botones y los empaquetamos en la ventana principal
boton1 = tk.Button(root, text="Botón 1")
boton1.pack(fill="both", expand=True)

boton2 = tk.Button(root, text="Botón 2")
boton2.pack(fill="both", expand=True)

root.mainloop()

import tkinter as tk

root = tk.Tk()
root.title("Ejemplo de Pack")

# Creamos una etiqueta y un botón y los empaquetamos en la ventana principal
etiqueta = tk.Label(root, text="¡Hola, Mundo!")
etiqueta.pack()

boton = tk.Button(root, text="Haz clic")
boton.pack()

root.mainloop()
